plugins {
    kotlin("jvm") version "1.9.23"
    `maven-publish`
}

group = "cz.ctu.fit.bi.kot.rational"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("mavenKotlin") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            // Specify the local repository path relative to the project directory
            url = uri("$rootDir/build/repo")
        }
    }
}
