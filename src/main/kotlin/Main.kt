package cz.ctu.fit.bi.kot.rational

class Ratio(_nom: Int, _denom: Int) {
    val nom: Int
    val denom: Int

    constructor(_nom: Int) : this(_nom, 1)

    companion object {
        tailrec
        fun gcd(n: Int, d: Int): Int =
            if (d == 0) n
            else gcd(d, n % d)
    }

    init {
        check(_denom != 0) { "zero denom" }
        val gcd = gcd(_nom, _denom)
        nom = _nom / gcd
        denom = _denom / gcd
    }

    override fun toString() = "$nom/$denom"

    operator fun times(ro: Ratio) = Ratio(nom * ro.nom, denom * ro.denom)
    operator fun times(ro: Int) = this * Ratio(ro)

    operator fun div(ro: Ratio) = this * ro.inv
    operator fun div(ro: Int) = inv * ro
    private val inv: Ratio by lazy {
        Ratio(denom, nom)
    }

    override fun equals(other: Any?) =
        if (this === other) true
        else if (other !is Ratio) false
        else  nom == other.nom && denom == other.denom

    override fun hashCode() = nom xor denom

}

operator fun Int.times(ro: Ratio) = Ratio(this) * ro
operator fun Int.div(ro: Ratio) = Ratio(this) * ro

fun main() {
    val r1 = Ratio(1, 2)
    val r2 = Ratio(2, 4)
    listOf(
        Ratio(1, 2),
        r1 * r2,
        r1 * 2,
        2 * r1
    ).forEach(::println)
}
